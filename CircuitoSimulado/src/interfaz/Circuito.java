package interfaz;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;

public class Circuito extends JFrame {
	
	boolean UP_PRESSED = false, DOWN_PRESSED = false, LEFT_PRESSED = false, RIGHT_PRESSED = false;
	
	JLabel cochePrueba = new JLabel("");
	Circuito thisCircuito = this;
	
	public Circuito() {
		
		setVisible(true);
		getContentPane().setLayout(null);
		
		cochePrueba = new JLabel("");
		cochePrueba.setIcon(new ImageIcon(Circuito.class.getResource("/recursos/cuche.png")));
		cochePrueba.setBounds(87, 65, 75, 122);
		getContentPane().add(cochePrueba);
		cochePrueba.setVisible(true);
		
		/*cochePrueba = new JLabel(new ImageIcon(Circuito.class.getResource("/recursos/cuche.png"))) {
			protected void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
									RenderingHints.VALUE_ANTIALIAS_ON);
			AffineTransform aT = g2.getTransform();
			Shape oldshape = g2.getClip();
			double x = getWidth()/2.0;
			double y = getHeight()/2.0;
			aT.rotate(Math.toRadians(30), x, y);
			g2.setTransform(aT);
			g2.setClip(oldshape);
			super.paintComponent(g);
		    }
		};*/
		
		Graphics2D cochePrueba2D = (Graphics2D)cochePrueba.getGraphics();
		cochePrueba2D.getTransform().rotate(Math.toRadians(30), cochePrueba.getWidth(), cochePrueba.getHeight());
		
		this.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				// ARRIBA
				if (evt.getKeyCode() == KeyEvent.VK_UP) {
					UP_PRESSED = true;
					System.out.println("UP");
					//rotarImagen(cochePrueba);
				}
				if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
					DOWN_PRESSED = true;
					System.out.println("DOWN");
				}
				if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
					LEFT_PRESSED = true;
					System.out.println("LEFT");
				}
				if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
					RIGHT_PRESSED = true;
					System.out.println("RIGHT");
				}
				
				if (UP_PRESSED)
					moverHaciaAdelante(cochePrueba);
				if (DOWN_PRESSED)
					moverHaciaAtras(cochePrueba);
				if (LEFT_PRESSED)
					moverHaciaIzquierda(cochePrueba);
				if (RIGHT_PRESSED)
					moverHaciaDerecha(cochePrueba);
			}
			
			public void keyReleased(java.awt.event.KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_UP) {
					UP_PRESSED = false;
				}
				if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
					DOWN_PRESSED = false;
				}
				if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
					LEFT_PRESSED = false;
				}
				if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
					RIGHT_PRESSED = false;
				}
			}
			
			
		});
		
	}
	
	private void rotarImagen(JLabel cochePrueba) {
		Graphics2D cochePrueba2D = (Graphics2D)cochePrueba.getGraphics();
		AffineTransform x = cochePrueba2D.getTransform();
		x.rotate(Math.toRadians(30), cochePrueba.getWidth(), cochePrueba.getHeight());
		Shape oldshape = cochePrueba2D.getClip();
		cochePrueba2D.setTransform(x);
		cochePrueba2D.setClip(oldshape);
		thisCircuito.paintComponents(cochePrueba.getGraphics());
	}
	
	final int DESPLAZAMIENTO = 5;
	
	private void moverHaciaAdelante(JLabel coche) {
		cochePrueba.setBounds(cochePrueba.getX(), cochePrueba.getY() + DESPLAZAMIENTO, cochePrueba.getWidth(), cochePrueba.getHeight());
	}
	
	private void moverHaciaAtras(JLabel coche) {
		cochePrueba.setBounds(cochePrueba.getX(), cochePrueba.getY() - DESPLAZAMIENTO, cochePrueba.getWidth(), cochePrueba.getHeight());
	}
	
	private void moverHaciaIzquierda(JLabel coche) {
		cochePrueba.setBounds(cochePrueba.getX() + DESPLAZAMIENTO, cochePrueba.getY(), cochePrueba.getWidth(), cochePrueba.getHeight());
	}
	
	private void moverHaciaDerecha(JLabel coche) {
		cochePrueba.setBounds(cochePrueba.getX() - DESPLAZAMIENTO, cochePrueba.getY(), cochePrueba.getWidth(), cochePrueba.getHeight());
	}
}
